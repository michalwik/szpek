using System;
using System.Collections.Generic;

namespace SzPEK.Models
{
    public class Participate
    {
        public int ParticipateID { get; set; }
        
        public string Name { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        
        public IList<CourseParticipate> CourseParticipates {get; set;}
    }
}