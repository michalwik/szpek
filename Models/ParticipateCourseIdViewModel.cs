using System;
using System.Collections.Generic;

namespace SzPEK.Models
{
    public class ParticipateCourseIdViewModel
    {
        public int CourseID {get; set;}
        public Participate Participate {get; set;}
    } 
}